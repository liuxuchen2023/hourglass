package utils

import (
	"crypto/md5"
	"encoding/hex"
)

func HashPassword(password string, username string) string {
	hash := md5.Sum([]byte(password + username)) //返回一个16字节的数组hash
	return hex.EncodeToString(hash[:])           //返回又字节数组转化成的16进制字符形式
}
