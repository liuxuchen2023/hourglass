package utils

import (
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
)

// const (
// 	duringTs = 60 * 60 * 24
// )

// 传输token设置cookie
func SetCookie(c *gin.Context, token string) {
	expiration := time.Now().Add(24 * time.Hour)
	cookie := &http.Cookie{
		Name:     "token",
		Value:    token,
		Expires:  expiration,
		Path:     "/",
		Domain:   "",
		Secure:   false,
		HttpOnly: true,
	}
	c.SetCookie(cookie.Name, cookie.Value, cookie.MaxAge, cookie.Path, cookie.Domain, cookie.Secure, cookie.HttpOnly)
}
