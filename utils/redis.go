package utils

import (
	"fmt"
	"hg/config"

	"github.com/go-redis/redis"
)

var client *redis.Client

func InitRedisClient() error {
	config:=config.GetConfig()
	client = redis.NewClient(&redis.Options{
		Addr:     config.ClientAddr,
		Password: config.ClientPassword,
		DB:       config.ClientDB,
	})

	// 检查连接是否成功
	pong, err := client.Ping().Result()
	if err != nil {
		return fmt.Errorf("failed to connect to Redis: %v", err)
	}
	fmt.Println("Connected to Redis:", pong)
	return nil
}

func GetRedis() *redis.Client {
	return client
}
