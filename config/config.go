package config

import (
	"fmt"
	"io/ioutil"

	"gopkg.in/yaml.v2"
)

type Config struct {
	Dbname          string `yaml:"dbname"`
	Dburl           string `yaml:"dburl"`
	Endpoint        string `yaml:"endpoint"`
	AccessKeyID     string `yaml:"accessKeyID"`
	SecretAccessKey string `yaml:"secretAccessKey"`
	UseSSL          bool   `yaml:"useSSL"`
	BucketName      string `yaml:"bucketName"`
	EmialHost       string `yaml:"emialHost"`
	EmialPort       int    `yaml:"emialPort"`
	EmialUserName   string `yaml:"emialUserName"`
	EmialPassword   string `yaml:"emialPassword"`
	NoticeURL       string `yaml:"noticeURL"`
	ClientAddr      string `yaml:"clientAddr"`
	ClientPassword  string `yaml:"clientPassword"`
	ClientDB        int    `yaml:"clientDB"`
}

var config = &Config{}

func GetConfig() *Config {
	return config
}

func ReadConfig() (*Config, error) {
	data, err := ioutil.ReadFile("config/config.yaml")
	if err != nil {
		fmt.Println("Read conf filed on conf package:", err)
		return nil, err
	}
	err = yaml.Unmarshal(data, config)
	if err != nil {
		fmt.Println("Read conf filed on conf package:", err)
		return nil, err
	}
	return config, nil
}
