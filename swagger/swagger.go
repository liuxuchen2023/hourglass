package swagger

type Response struct {
	Code int         `json:"code"`
	Msg  string      `json:"msg"`
	Data interface{} `json:"data"`
}

type Videos struct {
	Id        int    `json:"id"`
	VideoUrl  string `json:"videoUrl"`
	CoverUrl  string `json:"coverUrl"`
	Title     string `json:"title"`
	Reship    int    `json:"reship"`
	Label     string `json:"label"`
	Introduce string `json:"introduce"`
	Username  string `json:"username"`
	IsOffline int    `json:"isOffline"`
}

func GetDefaultResp() *Response {
	return &Response{
		Code: 0,
		Msg:  "OK",
	}
}
