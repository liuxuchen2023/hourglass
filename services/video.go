package services

import (
	"hg/models"
	"hg/pkg/database"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
)

// 通过id获取视频
func GetOneVideo(c *gin.Context) {
	videoIdString := c.Param("id")
	videoId, err := strconv.Atoi(videoIdString)
	if err != nil {
		logrus.Errorln("Transform string to int failed")
		return
	}
	data:= database.GetVideoinfoById(videoId)
	resp := models.GetDefaultResp()
	resp.Data = data
	c.JSON(http.StatusOK, resp)
}

//搜索视频
func SearchVideo(c *gin.Context) {
	searchRequest := new(models.SearchRequest)
	resp := models.GetDefaultResp()
	if err := c.ShouldBindJSON(searchRequest); err != nil {
		setApiError(c, http.StatusInternalServerError, resp, err)
		return
	}
	videos, cnt, _ := database.SearchVideo(searchRequest)
	resp.Data = videos
	resp.Count = cnt
	c.JSON(http.StatusOK, resp)
}
