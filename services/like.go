package services

import (
	"hg/models"
	"hg/pkg/database"
	"time"

	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
)

// 点赞视频
func LikeVideo(c *gin.Context) {
	user := getUserInfo(c)
	resp := models.GetDefaultResp()
	videoIDStr := c.Param("videoId")
	videoID, err := strconv.Atoi(videoIDStr)
	if err != nil {
		logrus.Errorln(err)
		c.JSON(http.StatusBadRequest, gin.H{"error": "video不存在"})
		return
	}
	like := new(models.Likes)
	if err := c.ShouldBindJSON(&like); err != nil {
		setApiError(c, http.StatusInternalServerError, resp, err)
		return
	}

	like.UserId = user.Id
	like.TargetId = videoID
	like.Time = time.Now()
	like.Type = 0
	database.InsertOneLike(like)
	c.JSON(http.StatusOK, resp)
}

// 取消点赞
func UnlikeVideo(c *gin.Context) {
	resp := models.GetDefaultResp()
	videoIDStr := c.Param("videoId")

	targetId, err := strconv.Atoi(videoIDStr)
	if err != nil {
		setApiError(c, http.StatusNotFound, resp, err)
		return
	}

	err = database.DeleteLike(targetId)
	if err != nil {
		setApiError(c, http.StatusInternalServerError, resp, err)
		return
	}

	c.JSON(http.StatusOK, resp)
}
