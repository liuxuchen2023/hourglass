package services

import (
	"hg/models"

	"github.com/gin-gonic/gin"
)

func setApiError(c *gin.Context, statusCode int, resp *models.Response, err error) {
	if resp.Msg == "OK" || resp.Msg == "" {
		resp.Code=400
		resp.Msg = err.Error()
	} else if resp.Msg != "" {
		resp.Code=400
		resp.Msg = resp.Msg + "," + err.Error()
	}
	c.AbortWithStatusJSON(statusCode, resp)
}
