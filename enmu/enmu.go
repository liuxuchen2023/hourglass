package enmu

const (
	Secret = 0
	Male   = 1
	Female = 2
)

// API CODE ERROR AND MSG
// LOGIN
const (
	BAD_LOGIN_INFO_CODE = 1000
	BAD_LOGIN_INFO_MSG  = "账号或密码错误"

	NO_AUTH_CODE_CODE = 1001
	NO_AUTH_CODE_MSG  = "没有验证码"

	BAD_AUTH_CODE_CODE = 1002
	BAD_AUTH_CODE_MSG  = "错误的验证码或已过期"
)

const (
	BAD_REGIRSTER_INFO_CODE = 1005
	BAD_REGIRSTER_INFO_MSG  = "用户名已被占用"
)

//json
const (
	EERROR_PROCESS_JSON_CODE= 1004
	EERROR_PROCESS_JSON_MSG  = "处理 JSON 数据时出现问题"
)

//file type

const (
	Picture =0
	Video =1
	ElseType=2	
)