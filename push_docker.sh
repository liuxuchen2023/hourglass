#!/bin/bash
set -e

DATE=`date +"%Y%m%d"`

bash scripts/git/tag.sh

VERSION=$(git describe --tags $(git rev-list --tags --max-count=1))

IMAGE=registry.cn-hangzhou.aliyuncs.com/liuyuzhuo/hg:${VERSION}

SCRIPT=$(readlink -f "$0")

SCRIPTPATH=$(dirname "${SCRIPT}")

echo "${VERSION}_${DATE}" > $SCRIPTPATH/VERSION.txt

docker build --build-arg version="${VERSION}_${DATE}" -t ${IMAGE}  ${SCRIPTPATH}

docker login --username=cspurple registry.cn-hangzhou.aliyuncs.com

docker push ${IMAGE}


# ssh root@43.143.128.23 "bash /xxx/run.sh"