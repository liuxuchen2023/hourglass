package main

import (
	"fmt"
	"hg/config"
	"hg/pkg/database"
	"hg/pkg/minio/storage"
	"hg/router"
	"hg/utils"

	_ "github.com/go-sql-driver/mysql"
)

// var cfg *string

func main() {
	// flag.StringVar(cfg, "config", "config/config.yaml", "config file path")
	// flag.Parse()
	//初始化配置文件
	// conf:=config.GetConfig()
	conf, err := config.ReadConfig()
	if err != nil {
		fmt.Println("Read conf failed:", err)
	}
	//初始化xorm引擎
	if err := database.CreateEngine(conf.Dbname, conf.Dburl); err != nil {
		fmt.Println("Create xorm engine failed:", err)
		return
	}
	//初始化redis
	if err := utils.InitRedisClient(); err != nil {
		fmt.Println("Create redis engine failed:", err)
		return
	}
	//初始化s3
	err = storage.NewS3Client(conf.Endpoint, conf.AccessKeyID, conf.SecretAccessKey, conf.UseSSL)
	if err != nil {
		fmt.Println("Create xorm s3 failed:", err)
	}
	//注册路由

	r := router.SetupRouters()

	r.Run(":31000")
}
