FROM golang:1.20.4 as golang

ENV GO111MODULE=on \
    CGO_ENABLED=1 \
    GOPROXY=https://goproxy.cn,direct

WORKDIR /build
ADD . /build

RUN CGO_ENABLED=0 GOOS=linux go build -ldflags '-w -s' -o hourglass

FROM alpine:latest

WORKDIR /app

# RUN apk add --no-cache bash
COPY --from=golang /build/hourglass /app
COPY ./config /app/config
COPY ./templates /app/templates
ENTRYPOINT ["/app/hourglass"]