#!/bin/bash

# 获取当前分支名
current_branch=$(git symbolic-ref --short HEAD)

latest_tag="0.0.0"
# 获取最新的tag，默认为0.0.0
latest_tag=$(git describe --tags $(git rev-list --tags --max-count=1) 2>/dev/null || echo "$latest_tag")

# 将tag按照"."分割成三个小版本
latest_tag=${latest_tag#"dev-"}
IFS='.' read -ra version_parts <<< "$latest_tag"
V1=${version_parts[0]}
V2=${version_parts[1]}
V3=${version_parts[2]}

# 根据发版的msg更新对应的小版本
msg=`cat .git/COMMIT_EDITMSG`
if [[ $msg == fix* ]]; then
  ((V3++))
elif [[ $msg == feat* ]]; then
  V3=0
  ((V2++))
elif [[ $msg == new* ]]; then
  V2=0
  V3=0
  ((V1++))
fi

if [[ $current_branch == "dev" ]]; then
  new_tag="dev-$V1.$V2.$V3"
else
  new_tag="$V1.$V2.$V3"
fi

# 在git中打一个新的tag
git tag $new_tag -m "$msg"

git push origin "$new_tag"
