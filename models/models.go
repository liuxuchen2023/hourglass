package models

import (
	"time"
)

type Barrage struct {
	Id       int    `xorm:"not null pk autoincr INT(11)"`
	VideoId  int    `xorm:"INT(11)"`
	UserId   int    `xorm:"INT(11)"`
	Content  string `xorm:"TEXT"`
	Time     int    `xorm:"INT(11)" json:"time"` // 时间，单位为十分之一秒
	Text     string `json:"text"`                // 弹幕文本内容
	Color    string `json:"color"`               // 弹幕颜色
	Position string `json:"position"`            // 弹幕位置 (0为滚动, 1为顶部, 2为底部)
	Size     string `json:"size"`                // 弹幕文字大小 (0为小字, 1为大字)
	IsNew    bool   `json:"isnew"`               // 弹幕是否为新发的
}

type SimpleBarrage struct {
	Time string `json:"time"` // 时间
	Text string `json:"text"` // 弹幕文本内容
}

type Comment struct {
	Id       int       `xorm:"not null pk autoincr INT(11)"`
	VideoId  int       `xorm:"INT(11)" json:"videoid"`
	UserId   int       `xorm:"INT(11)" json:"userid"`
	ParentId int       `xorm:"INT(11)" json:"parentid"`
	Content  string    `xorm:"TEXT" json:"content"`
	Time     time.Time `xorm:"DATETIME" json:"time"`
}

type EmailCodes struct {
	Id       int       `xorm:"not null pk autoincr INT(11)"`
	Username string    `xorm:"not null VARCHAR(255)"`
	Code     int       `xorm:"not null INT(11)"`
	Expiry   time.Time `xorm:"not null DATETIME"`
}

type Interaction struct {
	Id           int `xorm:"not null pk autoincr INT(11)"`
	VideoId      int `xorm:"INT(11)"`
	LikeCount    int `xorm:"INT(11)"`
	CommentCount int `xorm:"INT(11)"`
	ShareCount   int `xorm:"INT(11)"`
	CollectCount int `xorm:"INT(11)"`
}

type Likes struct {
	Id       int       `xorm:"not null pk autoincr INT(11)"`
	TargetId int       `xorm:"INT(11)"`
	Type     int       `xorm:"INT(11)"`
	UserId   int       `xorm:"INT(11)"`
	Time     time.Time `xorm:"DATETIME"`
}

type Object struct {
	Id         int       `xorm:"not null pk autoincr INT(11)"`
	ObjUrl     string    `xorm:"not null VARCHAR(255)"`
	Type       int       `xorm:"INT(11)"`
	UploadTime time.Time `xorm:"DATETIME"`
	DeleteTime time.Time `xorm:"DATETIME"`
}

type SchemaMigrations struct {
	Version int64 `xorm:"not null pk BIGINT(20)"`
	Dirty   int   `xorm:"not null TINYINT(1)"`
}

type Userinfo struct {
	Id        int       `xorm:"not null pk autoincr INT(11)" json:"id"`
	Username  string    `xorm:"not null unique VARCHAR(255)" json:"username"`
	Password  string    `xorm:"not null VARCHAR(255)" json:"password"`
	Email     string    `xorm:"VARCHAR(255)" json:"email"`
	Tel       string    `xorm:"VARCHAR(20)" json:"tel"`
	Gender    int       `xorm:"INT(11)" json:"gender"`
	Avater    string    `xorm:"VARCHAR(255)" json:"avater"`
	Birth     time.Time `xorm:"DATETIME" json:"birth"`
	Deletedat time.Time `xorm:"DATETIME" json:"deletedat"`
	Role      string    `xorm:"VARCHAR(50)" json:"role"`
}

type Videos struct {
	Id        int    `xorm:"not null pk autoincr INT(11)" json:"id"`
	ObjId     int    `xorm:"INT(11)" json:"objId"`
	Cover     int    `xorm:"INT(11)" json:"cover"`
	Title     string `xorm:"VARCHAR(255)" json:"title"`
	Reship    int    `xorm:"TINYINT(1)" json:"reship"`
	Label     string `xorm:"VARCHAR(255)" json:"label"`
	Introduce string `xorm:"TEXT" json:"introduce"`
	UserInfo  int    `xorm:"INT(11)" json:"userInfo"`
	IsOffline int    `xorm:"TINYINT(1)" json:"isOffline"`
}

type UserinfoToTable struct {
	Username string    `json:"username"`
	Id       int       `json:"id"`
	Gender   string    `json:"gender"`
	Birth    time.Time `json:"birth"`
	Region   string    `json:"region"`
}
