package models

type SearchRequest struct {
    Limit    int    `json:"limit"`
    Offset   int    `json:"offset"`
    OrderBy  string `json:"order_by"`
    Asc      bool   `json:"asc"`
    Name     string `json:"name"`
    Acc      bool   `json:"acc"`
}
