package models

type ResetPassword struct {
	Code     int    `json:"code"`
	Password string `json:"password"`
}


type NoticeMsg struct {
	Title    string   `json:"Title"`
	Content  string   `json:"Content"`
	UserNums []string `json:"UserNums"`
	Type     string   `json:"type"`
}