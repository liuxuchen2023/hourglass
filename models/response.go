package models

type Response struct {
	Code  int         `json:"code"`
	Msg   string      `json:"msg"`
	Data  interface{} `json:"data"`
	Count int         `json:"count"`
}

func GetDefaultResp() *Response {
	return &Response{
		Code: 0,
		Msg:  "OK",
	}
}
