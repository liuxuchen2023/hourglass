package router

import (
	"hg/pkg/middleware"
	"hg/services"

	"github.com/gin-gonic/gin"
)

func SetupRouters() *gin.Engine {
	r := gin.Default()
	r.Use(middleware.NoCache, middleware.Options, middleware.Secure)

	noAuth := r.Group("/v1")
	auth := r.Group("/v1")
	auth.Use(middleware.CheckLogin())
	noAuth.POST("/login", services.LoginPostService)
	noAuth.POST("/register", services.RegisterPostService)

	auth.POST("/upload", services.UploadToServer)
	auth.POST("/video", services.UploadVideoToCloudStorage)
	auth.POST("/image", services.UploadImageToCloudStorage)

	auth.GET("/auth_code", services.SendEmailCode)
	auth.PUT("/password", services.UpadatePassword)
	auth.PUT("/userinfo", services.UpdateUserinfo)
	//这个接口作用是获取视频详情页作者的个人信息
	auth.GET("/userinfo/video/:int", services.GetUserInfo)

	auth.GET("/videos/:id", services.GetOneVideo)
	noAuth.POST("/search/videos", services.SearchVideo)

	noAuth.POST("/danmu/:int", services.UrlToPostDanmu)
	noAuth.GET("/danmu/:int", services.UrlToGetDanmu)
	noAuth.GET("/barrage/:int", services.DanMu)
	noAuth.GET("/comment/:int", services.GetComment)
	auth.POST("/comment", services.AddComment)
	noAuth.DELETE("/comment/:int", services.DeleteComment)
	auth.POST("/like/:videoId", services.LikeVideo)
	noAuth.DELETE("/like/:videoId", services.UnlikeVideo)
	//获取用户详情也的用户信息
	auth.GET("/userinfo/table", services.GetUserInfoToTable)
	return r
}
