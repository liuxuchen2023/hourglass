package notice

import (
	"bytes"
	"encoding/json"
	"errors"
	"hg/config"
	"hg/models"
	"net/http"

	"github.com/sirupsen/logrus"
)

func SendNoticeToUser(noticeMsg *models.NoticeMsg) error {
	jsonData, err := json.Marshal(noticeMsg)
	if err != nil {
		logrus.Error("Failed to marshal json", err)
		return err
	}
	resp, err := http.Post(config.GetConfig().NoticeURL, "application/json", bytes.NewBuffer(jsonData))
	if err != nil {
		logrus.Error("Failed to psot notice", err)
		return err
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		logrus.Error("Notice request failed with status code", resp.StatusCode)
		return errors.New("notice request failed with status code")
	}
	return nil
}
