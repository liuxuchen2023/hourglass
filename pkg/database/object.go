package database

import (
	"hg/models"

	"github.com/sirupsen/logrus"
)

func InsertOneObject(object *models.Object) (*models.Object, error) {
	_, err := engine.InsertOne(object)
	if err != nil {
		logrus.Error(err)
		return nil, err
	}
	exist, err := engine.Get(object)
	if err != nil {
		logrus.Error(err)
		return nil, err
	}
	if !exist {
		return nil, err
	}
	return object, nil
}

func GetObjectInfoByObjectId(id int) (*models.Object, error) {
	object := &models.Object{Id: id}
	exist, err := engine.Where("id = ?", id).Get(object)
	if err != nil {
		logrus.Error(err)
		return nil, err
	}
	if !exist {
		return nil, ErrNotFound
	}
	return object, nil
}

func GetObjectInfoByObjectUrl(url string) (*models.Object, error) {
	object := &models.Object{ObjUrl: url}
	exist, err := engine.Where("url = ?", url).Get(object)
	if err != nil {
		logrus.Error(err)
		return nil, err
	}
	if !exist {
		return nil, ErrNotFound
	}
	return object, nil
}
