package database

import (
	"hg/models"

	"github.com/sirupsen/logrus"
	"xorm.io/xorm"
)

var engine *xorm.Engine
var xormErr error

func CreateEngine(Dbname, Dburl string) error {
	engine, xormErr = xorm.NewEngine(Dbname, Dburl)
	xormErr = engine.Sync2(new(models.Userinfo))
	if xormErr != nil {
		logrus.Errorln("Create engine filed:", xormErr)
	}
	return xormErr
}
