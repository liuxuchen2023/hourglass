package database

import (
	"hg/models"

	"github.com/sirupsen/logrus"
)

func InsertOneDanmu(danmu *models.Barrage) error{
	_, err := engine.InsertOne(danmu)
	if err != nil {
		logrus.Error("插入弹幕失败",err)
		return err
	}
	return nil
}

func GetAllBarrageByVideoID(videoId int) ([]models.Barrage, int,error) {
    var danmus []models.Barrage
    cnt,err := engine.Where("video_id = ?", videoId).OrderBy("time ASC").FindAndCount(&danmus)
    if err != nil {
		logrus.Errorln("查询所有弹幕失败：",err)
        return nil, 0,err
    }
    return danmus, int(cnt),nil
}