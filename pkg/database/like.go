package database

import (
	"hg/models"

	"github.com/sirupsen/logrus"
)

func InsertOneLike(like *models.Likes) error {
	_, err := engine.InsertOne(like)
	if err != nil {
		logrus.Error("点赞失败", err)
		return err
	}
	return nil
}

func DeleteLike(TargetId int) error {
	_, err := engine.Where("target_id = ?", TargetId).Delete(&models.Likes{})
	if err != nil {
		logrus.Errorln("取消点赞失败：", err)
		return err
	}
	return err
}
