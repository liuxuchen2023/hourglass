package database

import (
	"hg/models"

	"github.com/sirupsen/logrus"
	//"github.com/sirupsen/logrus"
)

func InsertOneComment(comment *models.Comment) error {
	_, err := engine.InsertOne(comment)
	if err != nil {
		logrus.Error("插入评论信息失败", err)
		return err
	}
	return nil
}

func GetCommentByVideoId(videoId int) ([]models.Comment, error) {
	var comments []models.Comment
	_, err := engine.Where("video_id = ?", videoId).OrderBy("time ASC").FindAndCount(&comments)
	if err != nil {
		logrus.Errorln("查询评论失败：", err)
		return nil, err
	}
	return comments, nil
}

func DeleteComment(commentId string) error {
	_, err := engine.Where("id = ?", commentId).Delete(&models.Comment{})
	if err != nil {
		logrus.Errorln("删除评论失败：", err)
		return err
	}
	return err
}
