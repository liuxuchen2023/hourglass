package database

import "errors"

var (
	ErrNotFound = errors.New("NOT FOUND")
)

var (
	ErrRegister = errors.New("用户名已被占用")
)
