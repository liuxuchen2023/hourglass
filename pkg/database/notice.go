package database

import (
	"hg/models"

	"github.com/sirupsen/logrus"
)

func InsertCodeToDB(code *models.EmailCodes) error {
	_, err := engine.InsertOne(code)
	return err
}

func VerifyEmailCode(user *models.Userinfo, inputCode int) bool {
	code := &models.EmailCodes{Username: user.Username}
	has, err := engine.Where(" username= ? AND expiry > NOW()", user.Username).Desc("expiry").Get(code)
	if err != nil {
		logrus.Error(err)
		return false
	}
	if !has || code.Code != inputCode {
        return false
    }
	return true
}
